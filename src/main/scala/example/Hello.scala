import java.io.{BufferedWriter, File, FileWriter}
import java.text.SimpleDateFormat
import scala.io.Source


object Hello {
  def main(args: Array[String]) {
    //println(System.getProperty("user.dir") + "/file.txt")
    val filename = System.getProperty("user.dir") + "/file.txt"
    var timestampNow = System.currentTimeMillis/1000
    var timestampNowMinusOneHour , timestampPlus600= (System.currentTimeMillis - 3600*1000)/1000

    // Mock
    val file = new File(filename)
    val bw = new BufferedWriter(new FileWriter(file))
    for( a <- 1 to 100){
      timestampPlus600 = timestampPlus600+500
      var hostOne = scala.util.Random
      bw.write(timestampPlus600 + " host" + hostOne.nextInt(10
      ) + " host"+
        hostOne.nextInt(10
        ) + "\n")
    }

    bw.close()

    var a=new Array[String](3)
    val mymultiarr= Array.ofDim[String](Source.fromFile(filename).getLines().size, 3)
    val timestampArr= new Array[Int](Source.fromFile(filename).getLines().size)
    val hostArr = new Array[String](Source.fromFile(filename).getLines().size)
    val hostCountArr = new Array[Int](Source.fromFile(filename).getLines().size)
    var cpt = 0
    var cpt_distinct = 0

    for (line <- Source.fromFile(filename).getLines ){
      a = line.split(" ")
      if (a(0).toInt <= timestampNow && a(0).toInt >= timestampNowMinusOneHour){
        // multi dim array
        mymultiarr(cpt)(0) = a(0)
        mymultiarr(cpt)(1) = a(1)
        mymultiarr(cpt)(2) = a(2)

        // timestamp
        timestampArr(cpt) = a(0).toInt
        var size = hostArr.length

        // hostnames and count
        if(!hostArr.contains(a(1))) { hostArr(cpt_distinct)= a(1) ; hostCountArr(cpt_distinct) = 1 ; cpt_distinct+=1}
        else {hostCountArr(hostArr.indexOf(a(1)))+=1}
        if(!hostArr.contains(a(2))) { hostArr(cpt_distinct)= a(2) ; hostCountArr(cpt_distinct) = 1 ; cpt_distinct+=1}
        else {hostCountArr(hostArr.indexOf(a(2)))+=1}

        cpt += 1
      }
    }

    // host list
    println("Liste des hôtes connectés pendant la dernière heure :")
    for(h <- hostArr)
    {
      if(h != null) { println(h) }
    }

    // last connection
    println("Serveurs avec la connexion la plus récente :")
    var max = (timestampArr.zipWithIndex.maxBy(_._1)._2)
    println(mymultiarr(max)(1) + " <=> " + mymultiarr(max)(2))

    // max connected 
    print("Serveur qui a généré le plus de connexion : ")
    max = (hostCountArr.zipWithIndex.maxBy(_._1)._2)
    print(hostArr(max))

  }

  // convert timestamp to date format
  def TimeStampToDateFomat (ts: String) : String = {
    val timestamp = ts.toLong*1000L
    val df = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss")
    return df.format(timestamp)
  }


}




